//
//  AppDelegate.h
//  HybridDribble
//
//  Created by Pedro Lopes on 17/04/18.
//  Copyright © 2018 Pedro Lopes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

